create table account
(
    id             char(36) primary key,
    account_number varchar(8),
    balance        numeric(15, 2),
    created_at     datetime(3),
    updated_at     datetime(3),
    constraint uq_account_accountNumber unique (account_number)
);

create table transaction
(
    id                       char(36) primary key,
    source_account_number      char(36) not null references account (account_number),
    destination_account_number char(36) not null references account (account_number),
    amount                   numeric(15, 2),
    created_at               datetime(3)
);