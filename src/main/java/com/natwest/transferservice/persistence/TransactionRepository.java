package com.natwest.transferservice.persistence;

import com.natwest.transferservice.persistence.entity.TransactionEntity;
import org.springframework.data.repository.CrudRepository;

public interface TransactionRepository extends CrudRepository<TransactionEntity, String> {

}
