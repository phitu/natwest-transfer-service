package com.natwest.transferservice.persistence;

import com.natwest.transferservice.persistence.entity.AccountEntity;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<AccountEntity, String> {

    AccountEntity findAccountEntityByAccountNumber(String accountNumber);
}
