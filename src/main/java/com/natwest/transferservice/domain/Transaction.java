package com.natwest.transferservice.domain;

import java.math.BigDecimal;

public class Transaction {

    private String sourceAccountNumber;
    private String destinationAccountNumber;
    private BigDecimal amount;

    public Transaction(String sourceAccountNumber, String destinationAccountNumber, BigDecimal amount) {
        this.sourceAccountNumber = sourceAccountNumber;
        this.destinationAccountNumber = destinationAccountNumber;
        this.amount = amount;
    }

    public String getSourceAccountNumber() {
        return sourceAccountNumber;
    }

    public String getDestinationAccountNumber() {
        return destinationAccountNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
