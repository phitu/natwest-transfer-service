package com.natwest.transferservice.service;

import com.natwest.transferservice.controller.dto.AccountCreationDto;
import com.natwest.transferservice.domain.Account;
import com.natwest.transferservice.exception.AccountNotFoundException;
import com.natwest.transferservice.exception.DuplicateAccountNumberException;
import com.natwest.transferservice.persistence.AccountRepository;
import com.natwest.transferservice.persistence.entity.AccountEntity;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

    private final AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public void createAccount(AccountCreationDto accountCreationDto) {
        if (doesAccountNumberExist(accountCreationDto.getAccountNumber())) {
            throw new DuplicateAccountNumberException();
        }
        AccountEntity accountEntity = mapToAccountEntity(accountCreationDto);
        accountRepository.save(accountEntity);
    }

    public AccountEntity getAccountEntity(String accountNumber) {
        AccountEntity accountEntity = accountRepository.findAccountEntityByAccountNumber(accountNumber);
        if (accountEntity != null) {
            return accountEntity;
        } else {
            throw new AccountNotFoundException();
        }
    }

    public List<Account> getAccounts() {
        List<AccountEntity> accounts = new ArrayList<>();
        accountRepository.findAll().iterator().forEachRemaining(accounts::add);
        return accounts.stream().map(a -> new Account(a.getAccountNumber(), a.getBalance()))
                .collect(Collectors.toList());
    }

    private AccountEntity mapToAccountEntity(AccountCreationDto accountCreationDto) {
        AccountEntity accountEntity = new AccountEntity();
        accountEntity.setId(UUID.randomUUID().toString());
        accountEntity.setAccountNumber(accountCreationDto.getAccountNumber());
        accountEntity.setBalance(accountCreationDto.getBalance());
        return accountEntity;
    }

    private boolean doesAccountNumberExist(String accountNumber) {
        if (accountRepository.findAccountEntityByAccountNumber(accountNumber) != null) {
            return true;
        }
        return false;
    }
}
