package com.natwest.transferservice.service;

import com.natwest.transferservice.controller.dto.TransactionDto;
import com.natwest.transferservice.domain.Transaction;
import com.natwest.transferservice.exception.InsufficientFundsException;
import com.natwest.transferservice.persistence.TransactionRepository;
import com.natwest.transferservice.persistence.entity.AccountEntity;
import com.natwest.transferservice.persistence.entity.TransactionEntity;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TransactionService {

    private final TransactionRepository transactionRepository;
    private final AccountService accountService;

    public TransactionService(TransactionRepository transactionRepository,
                              AccountService accountService) {
        this.transactionRepository = transactionRepository;
        this.accountService = accountService;
    }

    public void transferMoney(TransactionDto transactionDto) {
        AccountEntity sourceAccount = accountService.getAccountEntity(transactionDto.getSourceAccountNumber());
        AccountEntity destinationAccount = accountService
                .getAccountEntity(transactionDto.getDestinationAccountNumber());

        if (sourceAccount.getBalance().compareTo(transactionDto.getAmount()) < 0) {
            throw new InsufficientFundsException();
        }

        Transaction transaction = new Transaction(
                transactionDto.getSourceAccountNumber(),
                transactionDto.getDestinationAccountNumber(),
                transactionDto.getAmount()
        );
        updateAccountsAndSaveTransaction(transaction, sourceAccount, destinationAccount);
    }

    @Transactional
    void updateAccountsAndSaveTransaction(Transaction transaction,
                                          AccountEntity sourceAccount,
                                          AccountEntity destinationAccount) {
        TransactionEntity entity = mapToTransactionEntity(transaction);
        BigDecimal sourceAccCredited = sourceAccount.getBalance().subtract(transaction.getAmount());
        BigDecimal destinationAccCredited = destinationAccount.getBalance().add(transaction.getAmount());
        sourceAccount.setBalance(sourceAccCredited);
        destinationAccount.setBalance(destinationAccCredited);
        transactionRepository.save(entity);
    }

    public List<Transaction> getAllTransactions() {
        List<TransactionEntity> transactions = new ArrayList<>();
        transactionRepository.findAll().spliterator().forEachRemaining(transactions::add);
        return transactions.stream()
                .map(a -> new Transaction(a.getSourceAccountNumber(), a.getDestinationAccountNumber(), a.getAmount()))
                .collect(Collectors.toList());
    }

    private TransactionEntity mapToTransactionEntity(Transaction transaction) {
        TransactionEntity entity = new TransactionEntity();
        entity.setId(UUID.randomUUID().toString());
        entity.setSourceAccountNumber(transaction.getSourceAccountNumber());
        entity.setDestinationAccountNumber(transaction.getDestinationAccountNumber());
        entity.setAmount(transaction.getAmount());
        return entity;
    }
}
