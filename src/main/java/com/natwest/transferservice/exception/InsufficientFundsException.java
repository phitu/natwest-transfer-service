package com.natwest.transferservice.exception;

import com.natwest.transferservice.domain.ErrorDto;
import org.springframework.http.HttpStatus;

public class InsufficientFundsException extends AppException {

    @Override
    public ErrorDto getErrorDto() {
        return new ErrorDto("insufficient.funds", "Account does not have enough funds");
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.BAD_REQUEST;
    }
}
