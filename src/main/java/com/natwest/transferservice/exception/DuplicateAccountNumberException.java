package com.natwest.transferservice.exception;

import com.natwest.transferservice.domain.ErrorDto;
import org.springframework.http.HttpStatus;

public class DuplicateAccountNumberException extends AppException {

    @Override
    public ErrorDto getErrorDto() {
        return new ErrorDto("duplicate.account", "There is an existing account with the same account number");
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.BAD_REQUEST;
    }
}
