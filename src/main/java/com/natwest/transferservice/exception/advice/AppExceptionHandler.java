package com.natwest.transferservice.exception.advice;

import com.natwest.transferservice.domain.ErrorDto;
import com.natwest.transferservice.exception.AppException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AppExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(AppExceptionHandler.class);

    @ExceptionHandler(AppException.class)
    public ResponseEntity<ErrorDto> handleAppException(AppException exception) {
        LOG.error(exception.getMessage(), exception);
        return ResponseEntity
                .status(exception.getHttpStatus())
                .body(exception.getErrorDto());
    }
}
