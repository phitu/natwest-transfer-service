package com.natwest.transferservice.exception;

import com.natwest.transferservice.domain.ErrorDto;
import org.springframework.http.HttpStatus;

public class AccountNotFoundException extends AppException {

    @Override
    public ErrorDto getErrorDto() {
        return new ErrorDto("account.not.found", "Unable to find account with specified account number");
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_FOUND;
    }
}
