package com.natwest.transferservice.exception;

import com.natwest.transferservice.domain.ErrorDto;
import org.springframework.http.HttpStatus;

public abstract class AppException extends RuntimeException {

    public abstract ErrorDto getErrorDto();

    public abstract HttpStatus getHttpStatus();
}
