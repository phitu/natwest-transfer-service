package com.natwest.transferservice.controller;

import com.natwest.transferservice.controller.dto.AccountCreationDto;
import com.natwest.transferservice.controller.dto.AccountDto;
import com.natwest.transferservice.controller.dto.AccountResponseDto;
import com.natwest.transferservice.service.AccountService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/accounts")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping
    public ResponseEntity<Void> createAccount(@RequestBody AccountCreationDto accountCreationDto) {
        accountService.createAccount(accountCreationDto);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<AccountResponseDto> getAccounts() {

        List<AccountDto> accountDtoList = accountService.getAccounts().stream().map(a ->
                new AccountDto(a.getAccountNumber(), a.getBalance())).collect(Collectors.toList());

        AccountResponseDto accountResponseDto = new AccountResponseDto();
        accountResponseDto.setAccounts(accountDtoList);
        return ResponseEntity.ok(accountResponseDto);
    }
}
