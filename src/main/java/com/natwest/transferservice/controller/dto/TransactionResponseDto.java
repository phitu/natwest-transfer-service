package com.natwest.transferservice.controller.dto;

import java.util.ArrayList;
import java.util.List;

public class TransactionResponseDto {

    private List<TransactionDto> transactions = new ArrayList<>();

    public List<TransactionDto> getTransactions() {
        return transactions;
    }

    public void setTransactions(
            List<TransactionDto> transactionDtoList) {
        this.transactions = transactionDtoList;
    }
}