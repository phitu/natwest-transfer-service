package com.natwest.transferservice.controller.dto;

import java.math.BigDecimal;

public class TransactionDto {

    private String sourceAccountNumber;
    private String destinationAccountNumber;
    private BigDecimal amount;

    public TransactionDto(String sourceAccountNumber, String destinationAccountNumber, BigDecimal amount) {
        this.sourceAccountNumber = sourceAccountNumber;
        this.destinationAccountNumber = destinationAccountNumber;
        this.amount = amount;
    }

    public String getSourceAccountNumber() {
        return sourceAccountNumber;
    }

    public String getDestinationAccountNumber() {
        return destinationAccountNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
