package com.natwest.transferservice.controller.dto;

import java.util.ArrayList;
import java.util.List;

public class AccountResponseDto {

    private List<AccountDto> accounts = new ArrayList<>();

    public List<AccountDto> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountDto> accountNumber) {
        this.accounts = accountNumber;
    }
}

