package com.natwest.transferservice.controller;

import com.natwest.transferservice.controller.dto.TransactionDto;
import com.natwest.transferservice.controller.dto.TransactionResponseDto;
import com.natwest.transferservice.service.TransactionService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/transactions")
public class TransactionController {

    private final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping
    public ResponseEntity<Void> transferMoney(@RequestBody TransactionDto transactionDto) {
        transactionService.transferMoney(transactionDto);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<TransactionResponseDto> getTransactions() {
        TransactionResponseDto transactionResponseDto = new TransactionResponseDto();
        List<TransactionDto> transactionDtos = transactionService.getAllTransactions().stream()
                .map(a -> new TransactionDto(a.getSourceAccountNumber(), a.getDestinationAccountNumber(),
                        a.getAmount()))
                .collect(Collectors.toList());
        transactionResponseDto.setTransactions(transactionDtos);
        return ResponseEntity.ok(transactionResponseDto);
    }
}
