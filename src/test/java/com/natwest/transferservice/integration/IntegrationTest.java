package com.natwest.transferservice.integration;

import static io.restassured.config.JsonConfig.jsonConfig;
import static io.restassured.config.RestAssuredConfig.newConfig;
import static org.testcontainers.containers.MySQLContainer.MYSQL_PORT;

import com.natwest.transferservice.integration.IntegrationTest.MySqlTestContainerInitializer;
import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.path.json.config.JsonPathConfig.NumberReturnType;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.ContainerLaunchException;
import org.testcontainers.containers.MySQLContainer;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = MySqlTestContainerInitializer.class)
public class IntegrationTest {
    RestAssuredConfig config = newConfig()
            .jsonConfig(jsonConfig().numberReturnType(NumberReturnType.BIG_DECIMAL));
    public static MySQLContainer mysql = new MySQLContainer("mysql:5.7.26");

    static {
        mysql.addExposedPort(MYSQL_PORT);
        mysql.addEnv("MYSQL_DATABASE", mysql.getDatabaseName());
        mysql.addEnv("MYSQL_USER", mysql.getUsername());
        if (mysql.getPassword() != null && !mysql.getPassword().isEmpty()) {
            mysql.addEnv("MYSQL_PASSWORD", mysql.getPassword());
            mysql.addEnv("MYSQL_ROOT_PASSWORD", mysql.getPassword());
        } else if ("root".equals(mysql.getUsername())) {
            mysql.addEnv("MYSQL_ALLOW_EMPTY_PASSWORD", "yes");
        } else {
            throw new ContainerLaunchException("Empty password can be used only with the root user");
        }
        mysql.setStartupAttempts(3);
        mysql.start();
    }

    static class MySqlTestContainerInitializer implements
            ApplicationContextInitializer<ConfigurableApplicationContext> {

        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + mysql.getJdbcUrl(),
                    "spring.datasource.username=" + mysql.getUsername(),
                    "spring.datasource.password=" + mysql.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }

    @LocalServerPort
    protected int port = 0;

    
}
