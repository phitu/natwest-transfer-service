package com.natwest.transferservice.integration;

import static io.restassured.RestAssured.given;
import static io.restassured.config.JsonConfig.jsonConfig;
import static io.restassured.config.RestAssuredConfig.newConfig;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.natwest.transferservice.controller.dto.AccountCreationDto;
import com.natwest.transferservice.persistence.AccountRepository;
import com.natwest.transferservice.persistence.entity.AccountEntity;
import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.path.json.config.JsonPathConfig.NumberReturnType;
import java.math.BigDecimal;
import java.util.UUID;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.testcontainers.shaded.com.google.common.collect.Iterables;

public class AccountIntegrationTest extends IntegrationTest {

    @Autowired
    private AccountRepository accountRepository;

    @BeforeEach
    void setUp() {
        accountRepository.deleteAll();
    }

    @Test
    public void shouldCreateAccountSuccessfully() {
        AccountCreationDto accountCreationDto = new AccountCreationDto("9000", BigDecimal.valueOf(12.23));

        given().baseUri("http://localhost:" + port)
                .header("Content-Type", "application/json")
                .body(accountCreationDto)
                .post("v1/accounts")
                .then().statusCode(204);

        Iterable<AccountEntity> allAccounts = accountRepository.findAll();
        assertThat(Iterables.size(allAccounts)).isEqualTo(1);
        AccountEntity accountEntity = allAccounts.iterator().next();
        assertThat(accountEntity.getAccountNumber()).isEqualTo("9000");
        assertThat(accountEntity.getBalance()).isEqualTo(BigDecimal.valueOf(12.23));
    }

    @Test
    public void shouldGetAccount() {
        AccountEntity accountEntity = new AccountEntity();
        accountEntity.setId(UUID.randomUUID().toString());
        accountEntity.setAccountNumber("2000");
        accountEntity.setBalance(BigDecimal.valueOf(12.22));
        
        accountRepository.save(accountEntity);
        given().config(config).baseUri("http://localhost:" + port)
                .header("Content-Type", "application/json")
                .get("v1/accounts")
                .then()
                .statusCode(200)
                .body("accounts.size()", Matchers.is(1),
                        "accounts[0].accountNumber", Matchers.is("2000"),
                        "accounts[0].balance", Matchers.is(BigDecimal.valueOf(12.22)));
    }
}
