package com.natwest.transferservice.integration;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.google.common.collect.Iterables;
import com.natwest.transferservice.controller.dto.TransactionDto;
import com.natwest.transferservice.persistence.AccountRepository;
import com.natwest.transferservice.persistence.TransactionRepository;
import com.natwest.transferservice.persistence.entity.AccountEntity;
import com.natwest.transferservice.persistence.entity.TransactionEntity;
import java.math.BigDecimal;
import java.util.UUID;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TransactionIntegrationTest extends IntegrationTest {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private AccountRepository accountRepository;

    @BeforeEach
    void setUp() {
        transactionRepository.deleteAll();
        accountRepository.deleteAll();
    }

    @Test
    public void shouldSuccessfullyTransferMoney() {
        AccountEntity sourceAccount = new AccountEntity();
        sourceAccount.setId(UUID.randomUUID().toString());
        sourceAccount.setAccountNumber("1234");
        sourceAccount.setBalance(BigDecimal.valueOf(123.45));
        accountRepository.save(sourceAccount);
        AccountEntity destinationAccount = new AccountEntity();
        destinationAccount.setId(UUID.randomUUID().toString());
        destinationAccount.setAccountNumber("41234");
        destinationAccount.setBalance(BigDecimal.valueOf(23.43));
        accountRepository.save(destinationAccount);
        TransactionDto transactionDto = new TransactionDto(
                "1234", "41234", BigDecimal.valueOf(12.23));

        given().baseUri("http://localhost:" + port)
                .header("Content-Type", "application/json")
                .body(transactionDto)
                .post("v1/transactions")
                .then().statusCode(204);

        AccountEntity sourceAccountEntity = accountRepository.findAccountEntityByAccountNumber("1234");
        assertThat(sourceAccountEntity.getBalance()).isEqualTo(BigDecimal.valueOf(111.22));
        AccountEntity destAccountEntity = accountRepository.findAccountEntityByAccountNumber("41234");
        assertThat(destAccountEntity.getBalance()).isEqualTo(BigDecimal.valueOf(35.66));
        TransactionEntity transactionEntity = transactionRepository.findAll().iterator().next();
        assertThat(transactionEntity.getSourceAccountNumber()).isEqualTo("1234");
        assertThat(transactionEntity.getDestinationAccountNumber()).isEqualTo("41234");
        assertThat(transactionEntity.getAmount()).isEqualTo(BigDecimal.valueOf(12.23));
    }

    @Test
    public void shouldThrow404ErrorIfAccountNotFound() {

        TransactionDto transactionDto = new TransactionDto(
                "1234", "41234", BigDecimal.valueOf(12.23));

        given().baseUri("http://localhost:" + port)
                .header("Content-Type", "application/json")
                .body(transactionDto)
                .post("v1/transactions")
                .then()
                .statusCode(404)
                .body("errorCode", Matchers.is("account.not.found"),
                        "message", Matchers.is("Unable to find account with specified account number"));

        Iterable<TransactionEntity> transactionEntities = transactionRepository.findAll();
        assertThat(Iterables.size(transactionEntities)).isEqualTo(0);
    }

    @Test
    public void shouldThrow400ErrorIfAccountHasInsufficientFundsForTransaction() {
        AccountEntity sourceAccount = new AccountEntity();
        sourceAccount.setId(UUID.randomUUID().toString());
        sourceAccount.setAccountNumber("1234");
        sourceAccount.setBalance(BigDecimal.valueOf(3.45));
        accountRepository.save(sourceAccount);
        AccountEntity destinationAccount = new AccountEntity();
        destinationAccount.setId(UUID.randomUUID().toString());
        destinationAccount.setAccountNumber("41234");
        destinationAccount.setBalance(BigDecimal.valueOf(23.43));
        accountRepository.save(destinationAccount);
        TransactionDto transactionDto = new TransactionDto(
                "1234", "41234", BigDecimal.valueOf(12.23));

        given().baseUri("http://localhost:" + port)
                .header("Content-Type", "application/json")
                .body(transactionDto)
                .post("v1/transactions")
                .then()
                .statusCode(400)
                .body("errorCode", Matchers.is("insufficient.funds"),
                        "message", Matchers.is("Account does not have enough funds"));

        Iterable<TransactionEntity> transactionEntities = transactionRepository.findAll();
        assertThat(Iterables.size(transactionEntities)).isEqualTo(0);
    }
    
    @Test
    public void shouldGetTransaction() {
        AccountEntity sourceAccount = new AccountEntity();
        sourceAccount.setId(UUID.randomUUID().toString());
        sourceAccount.setAccountNumber("1234");
        sourceAccount.setBalance(BigDecimal.valueOf(3.45));
        accountRepository.save(sourceAccount);
        AccountEntity destinationAccount = new AccountEntity();
        destinationAccount.setId(UUID.randomUUID().toString());
        destinationAccount.setAccountNumber("41234");
        destinationAccount.setBalance(BigDecimal.valueOf(23.43));
        accountRepository.save(destinationAccount);
        
        TransactionEntity transactionEntity = new TransactionEntity();
        transactionEntity.setId(UUID.randomUUID().toString());
        transactionEntity.setSourceAccountNumber("1234");
        transactionEntity.setDestinationAccountNumber("41234");
        transactionEntity.setAmount(BigDecimal.valueOf(2.32));
        transactionRepository.save(transactionEntity);
        
        given().config(config).baseUri("http://localhost:" + port)
                .header("Content-Type", "application/json")
                .get("v1/transactions")
                .then()
                .statusCode(200)
                .body("transactions.size()", Matchers.is(1),
                        "transactions[0].sourceAccountNumber", Matchers.is("1234"),
                        "transactions[0].destinationAccountNumber", Matchers.is("41234"),
                        "transactions[0].amount", Matchers.is(BigDecimal.valueOf(2.32)));

    }
}
