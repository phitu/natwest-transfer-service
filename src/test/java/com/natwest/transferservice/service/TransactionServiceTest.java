package com.natwest.transferservice.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import com.google.common.collect.Iterables;
import com.natwest.transferservice.controller.dto.TransactionDto;
import com.natwest.transferservice.domain.Transaction;
import com.natwest.transferservice.exception.InsufficientFundsException;
import com.natwest.transferservice.persistence.TransactionRepository;
import com.natwest.transferservice.persistence.entity.AccountEntity;
import com.natwest.transferservice.persistence.entity.TransactionEntity;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TransactionServiceTest {

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private AccountService accountService;
    
    @InjectMocks
    private TransactionService transactionService;
    
    @Test
    void shouldSuccessfullyTransferMoneyUpdateAccountsAndCreateTransactionLog() {
        AccountEntity sourceAcc = mock(AccountEntity.class);
        when(sourceAcc.getBalance()).thenReturn(BigDecimal.valueOf(32));
        AccountEntity destAcc = mock(AccountEntity.class);
        when(destAcc.getBalance()).thenReturn(BigDecimal.valueOf(32));
        
        when(accountService.getAccountEntity("12")).thenReturn(sourceAcc);
        when(accountService.getAccountEntity("34")).thenReturn(destAcc);
        transactionService.transferMoney(new TransactionDto(
                "12", "34", BigDecimal.TEN
        ));

        ArgumentCaptor<TransactionEntity> captor = ArgumentCaptor.forClass(TransactionEntity.class);
        verify(transactionRepository).save(captor.capture());
        assertThat(captor.getValue().getSourceAccountNumber()).isEqualTo("12");
        assertThat(captor.getValue().getDestinationAccountNumber()).isEqualTo("34");
        assertThat(captor.getValue().getAmount()).isEqualTo(BigDecimal.TEN);
        verify(sourceAcc).setBalance(BigDecimal.valueOf(22));
        verify(destAcc).setBalance(BigDecimal.valueOf(42));
    }

    @Test
    void shouldThrowInsufficientFundsExceptionIfSourceAccountDoesNotHaveEnoughFundsForTransaction() {
        AccountEntity sourceAcc = new AccountEntity();
        sourceAcc.setBalance(BigDecimal.valueOf(2));
        AccountEntity destAcc = new AccountEntity();
        destAcc.setBalance(BigDecimal.valueOf(32));

        when(accountService.getAccountEntity("12")).thenReturn(sourceAcc);
        when(accountService.getAccountEntity("34")).thenReturn(destAcc);
        
        assertThrows(InsufficientFundsException.class, () -> {
            transactionService.transferMoney(new TransactionDto(
                    "12", "34", BigDecimal.TEN
            ));
        });
        
        verifyNoInteractions(transactionRepository);
    }

    @Test
    void shouldGetAllTransactions() {
        List<TransactionEntity> transactionEntities = List.of(new TransactionEntity());
        when(transactionRepository.findAll()).thenReturn(transactionEntities);

        List<Transaction> allTransactions = transactionService.getAllTransactions();
        assertThat(allTransactions.size()).isEqualTo(1);
    }
}