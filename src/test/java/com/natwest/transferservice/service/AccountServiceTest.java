package com.natwest.transferservice.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.natwest.transferservice.controller.dto.AccountCreationDto;
import com.natwest.transferservice.domain.Account;
import com.natwest.transferservice.exception.AccountNotFoundException;
import com.natwest.transferservice.exception.DuplicateAccountNumberException;
import com.natwest.transferservice.persistence.AccountRepository;
import com.natwest.transferservice.persistence.entity.AccountEntity;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {

    @Mock
    private AccountRepository accountRepository;
    
    @InjectMocks
    private AccountService accountService;
    
    @Test
    void shouldCreateAccount() {
        when(accountRepository.findAccountEntityByAccountNumber(any())).thenReturn(null);
        
        accountService.createAccount(new AccountCreationDto("8774", BigDecimal.TEN));
        
        ArgumentCaptor<AccountEntity> captor = ArgumentCaptor.forClass(AccountEntity.class);
        verify(accountRepository).save(captor.capture());
        assertThat(captor.getValue().getAccountNumber()).isEqualTo("8774");
        assertThat(captor.getValue().getBalance()).isEqualTo(BigDecimal.TEN);
    }

    @Test
    void shouldThrowDuplicateAccountNumberExceptionIfAccountExists() {
        when(accountRepository.findAccountEntityByAccountNumber(any())).thenReturn(new AccountEntity());
        assertThrows(DuplicateAccountNumberException.class, () -> {
            accountService.createAccount(new AccountCreationDto("8774", BigDecimal.TEN));  
        });
        
        verify(accountRepository, never()).save(any());
    }
    
    @Test
    void shouldGetAccountEntity() {
        when(accountRepository.findAccountEntityByAccountNumber(any())).thenReturn(new AccountEntity());

        AccountEntity accountEntity = accountService.getAccountEntity("1234");
        
        assertThat(accountEntity).isNotNull();
    }
    
    @Test
    void shouldThrowAccountNotFoundExceptionIfAccountNotFound() {
        when(accountRepository.findAccountEntityByAccountNumber(any())).thenReturn(null);

        assertThrows(AccountNotFoundException.class, () -> {
            accountService.getAccountEntity("1234");
        });
    }
    
    @Test
    void shouldGetListOfAccounts() {
        when(accountRepository.findAll()).thenReturn(List.of(new AccountEntity()));

        List<Account> accounts = accountService.getAccounts();
        
        assertThat(accounts.size()).isEqualTo(1);
    }
}