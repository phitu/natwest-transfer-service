# Transfer Service

Restful API service for doing transfers between accounts.

#### Build Requirements
Built using Java 11 SDK, Spring Boot, TestContainers for integration tests

#### Build
Run ` ./gradlew clean build` to build a jar located in `build/libs`

#### Run Program
Run `java -jar build/libs/transfer-service-0.0.1-SNAPSHOT.jar`

Use postman collections located in `src/postman-collections`
Endpoints available
* GET /accounts get all accounts
* POST /accounts create account
* GET /transactions get all transactions
* POST /transactions transfer money